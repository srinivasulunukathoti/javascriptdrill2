// Convert all the salary values into proper numbers instead of strings.
function convertAllSalariesProperNumbers(data) {
    //check the data properly given correct or not
    if (Array.isArray(data)) {
        let salariesInNumbers = [];
        for (let index = 0; index < data.length; index++) {
            const salaries = Number(data[index].salary.replace("$",""));
            salariesInNumbers.push(salaries);
        }
        return salariesInNumbers;
    } else {
        return "Insufficiant data";
    }
}
//exports the code
module.exports = {convertAllSalariesProperNumbers};