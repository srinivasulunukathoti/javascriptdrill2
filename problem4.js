// Find the sum of all salaries.
function findSumOfAllSalaries(data) {
    //check the data properly given correct or not
    if (Array.isArray(data)) {
        let sumOfAllSalaries = 0;
        for (let index = 0; index < data.length; index++) {
            //converting number
            const salary = Number(data[index].salary.replace("$",""));
            sumOfAllSalaries+=(salary*10000);
        }
        return sumOfAllSalaries;
    } else {
        return "data not found";
    }
}
//exports the code
module.exports = {findSumOfAllSalaries};