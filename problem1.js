// Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )
function findAllWebDevelopers(data) {
    //check the data properly given correct or not
    if (Array.isArray(data)) {
        let info = [];
        for (let index = 0; index < data.length; index++) {
            const job = data[index].job;
            if (job.includes("Web Developer")) {
                info[data[index].first_name + " "+ data[index].last_name] = data[index].job;
            }
        }
        return info;
    } else {
        return "data insufficient";
    }
}
//exports the code
module.exports = {findAllWebDevelopers};