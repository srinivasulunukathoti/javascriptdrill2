// Find the average salary of based on country. ( Groupd it based on country and then find the average ).

function eachCountryAverageSalary(data) {
    //check the data properly given correct or not
    if (Array.isArray(data)) {
        // to store average salary
        const averageSalary = [];
        for (let index = 0; index < data.length; index++) {
            //finding the location
            let country = data[index].location;
            let sum =0;
            let count =0;
            for (let index1 = 0; index1 < data.length; index1++) {
                let location = data[index1].location;
                //check data is true or false
                if (location === country) {
                    const salary = Number(data[index1].salary.replace("$",""));
                    sum+=(salary*10000);
                    count++;
                }
                
            }
            //finding average salary of country
            let average = sum / count;
            averageSalary.push({country, average})
        }
        return averageSalary;
    } else {
        return "data not found";
    }
}
//exports the code
module.exports = {eachCountryAverageSalary};