// Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).

function salarySumBasedOnCountry(data) {
    //check the data properly given correct or not
    if (Array.isArray(data)) {
        const salarysumGroupOfCountry = [];
        for (let index = 0; index < data.length; index++) {
            let country = data[index].location;
            let sum =0;
            for (let index1 = 0; index1 < data.length; index1++) {
                let location = data[index].location;
                //check data is true or false
                if (location === country) {
                    const salary = Number(data[index].salary.replace("$",""));
                    sum+=(salary*10000);
                }
                
            }
            salarysumGroupOfCountry.push({country,sum});
        }
        return salarysumGroupOfCountry;
    } else {
        return "data not found";
    }
}
//exports the code
module.exports = {salarySumBasedOnCountry};
