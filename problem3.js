function correctAllSalaries(data) {
    //check the data properly given correct or not
    if (Array.isArray(data)) {
        const correctedSalaries = [];
        for (let index = 0; index < data.length; index++) {
            const salary = Number(data[index].salary.replace("$",""));
            const correctedSalary = salary * 10000;
            const salaryData = {...data[index], corrected_salary:correctedSalary};
            correctedSalaries.push(salaryData);
        }
        return correctedSalaries;
    } else {
        return "data not found"
    }
}
//exports the code
module.exports = {correctAllSalaries};